export const DIRECTIONS = {
  EAST: 'E',
  WEST: 'W',
  NORTH: 'N',
  SOUTH: 'S'
};

export const DIRECTION_CLOCK = [
  DIRECTIONS.NORTH,
  DIRECTIONS.EAST,
  DIRECTIONS.SOUTH,
  DIRECTIONS.WEST
];

export const INSTRUCTION = {
  RIGHT: 'R',
  LEFT: 'L',
  FORWARD: 'F'
}
