import { ActionTypes, Reducer } from '../robots';
import { parsedInput } from '../../testData/inputs';
import { DIRECTIONS } from '../../AppConstants';

describe('robots module', () => {
  describe('reducer', () => {
    const getState = (executedRobots = [], robotsToExecute = parsedInput.robots) => ({
      executedRobots,
      robotsToExecute
    });
  
    test('setting robot final Position, should remove robot from execution and add to executed collection', () => {
      const robotFinalPosition = { x: 10, y: 20, direction: DIRECTIONS.EAST };
      const { robotsToExecute } = getState();

      expect(
        Reducer(
          getState(),
          {
            type: ActionTypes.SET_ROBOT_FINAL_POSITION,
            robotFinalPosition
          }
        )
      ).toMatchSnapshot();
    });

    test('ajusting robot Position, should remove next instruction and adjust position', () => {
      const position = { x: 1, y: 1, direction: DIRECTIONS.SOUTH };
      const { robotsToExecute, executedRobots } = getState();
      const [ robotToAdjust, ...nextRobots ] = robotsToExecute;

      expect(
        Reducer(
          getState(),
          {
            type: ActionTypes.ADJUST_ROBOT_POSITION,
            position
          }
        )
      ).toMatchSnapshot();
    });
  });
});
