import { Selectors, Reducer, ActionTypes } from '../grid';
import { DIRECTIONS } from '../../AppConstants';

describe('grid module', () => {
  describe('Selectors', () => {
    const getState = (override = {}) => ({
      grid: {
        maxX: 10,
        maxY: 20,
        minX: 0,
        minY: 0,
        blackListedCoordinates: [ { x: 10, y: 5, direction: DIRECTIONS.NORTH } ],
        ...override
      }
    });

    describe('isInsideGridBounderies', () => {
      test('should be true inside bounderies', () => {
        expect(
          Selectors.isInsideGridBounderies(getState(), { x: 10, y: 20 })
        ).toBeTruthy();

        expect(
          Selectors.isInsideGridBounderies(getState(), { x: 0, y: 20 })
        ).toBeTruthy();

        expect(
          Selectors.isInsideGridBounderies(getState(), { x: 0, y: 0 })
        ).toBeTruthy();
      });

      test('should be false outside bounderies', () => {
        expect(
          Selectors.isInsideGridBounderies(getState(), { x: 5, y: -1 })
        ).toBeFalsy();

        expect(
          Selectors.isInsideGridBounderies(getState(), { x: 5, y: 21 })
        ).toBeFalsy();

        expect(
          Selectors.isInsideGridBounderies(getState(), { x: 11, y: 2 })
        ).toBeFalsy();

        expect(
          Selectors.isInsideGridBounderies(getState(), { x: -1, y: 2 })
        ).toBeFalsy();
      });
    });

    describe('isMovementBlackListed', () => {
      it('should be true when a coordinate is black marked for a certain direction', () => {
        expect(
          Selectors.isMovementBlackListed(
            getState(),
            { x: 10, y: 5, direction: DIRECTIONS.NORTH }
          )
        ).toBe(true);
      });

      it('should be false when a coordinate is not black marked for a certain direction', () => {
        expect(
          Selectors.isMovementBlackListed(
            getState(),
            { x: 10, y: 5, direction: DIRECTIONS.WEST }
          )
        ).toBe(false);

        expect(
          Selectors.isMovementBlackListed(
            getState(),
            { x: 10, y: 21, direction: DIRECTIONS.WEST }
          )
        ).toBe(false);
      });
    });
  });

  describe('Reducer', () => {
    test('should set dimensions', () => {
      expect(
        Reducer({}, { type: ActionTypes.SET_DIMENSIONS, numberOfColumns: 2, numberOfRows: 5 })
      ).toMatchSnapshot();
    });
  });
});
