import { isEqual } from 'lodash';

import { reducerFactory } from '../utils';
import { INSTRUCTION } from '../AppConstants';

export const ActionTypes = {
  SET_DIMENSIONS: 'SET_DIMENSIONS',
  SET_BLACK_LISTED_COORDINATE: 'SET_BLACK_LISTED_COORDINATE'
};

export const ActionCreators = {
  setDimensions: dimensions => ({
    ...dimensions,
    type: ActionTypes.SET_DIMENSIONS,
  }),
  setBlackListedCoordinate: coordinates => ({
    coordinates,
    type: ActionTypes.SET_BLACK_LISTED_COORDINATE
  })
};

const reduceMap = {
  [ActionTypes.SET_DIMENSIONS]: (s, { numberOfColumns, numberOfRows }) => ({
    ...s,
    minY: 0,
    minX: 0,
    maxY: numberOfRows,
    maxX: numberOfColumns
  }),
  [ActionTypes.SET_BLACK_LISTED_COORDINATE]: (s, { coordinates }) => ({
    ...s,
    blackListedCoordinates: [ ...s.blackListedCoordinates, coordinates ]
  })
}

const getGrid = state => state.grid;

const isInsideGridBounderies = (state, { x, y }) => {
  const { maxX, maxY, minX, minY } = getGrid(state);

  return x >= minX && x <= maxX && y >= minY && y <= maxY;
}

const isMovementBlackListed = (state, coordinates, instruction) => {
  const { blackListedCoordinates } = getGrid(state);
  
  return blackListedCoordinates.some(c => isEqual(c, coordinates));
};

export const Selectors = {
  isMovementBlackListed,
  getGrid,
  isInsideGridBounderies
};

export const Reducer = reducerFactory({ blackListedCoordinates: [] }, reduceMap);
