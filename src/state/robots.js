import { reducerFactory } from "../utils";

export const ActionTypes = {
  SET_ROBOTS_TO_EXECUTE: 'SET_ROBOTS_TO_EXECUTE',
  ADJUST_ROBOT_POSITION: 'ADJUST_ROBOT_POSITION',
  SET_ROBOT_FINAL_POSITION: 'SET_ROBOT_FINAL_POSITION'
};

export const ActionCreators = {
  setRobotsToExecute: robotsToExecute => ({
    robotsToExecute,
    type: ActionTypes.SET_ROBOTS_TO_EXECUTE,
  }),
  setFinalPosition: robotFinalPosition => ({
    robotFinalPosition,
    type: ActionTypes.SET_ROBOT_FINAL_POSITION
  }),
  ajustCurrentPosition: position => ({
    position,
    type: ActionTypes.ADJUST_ROBOT_POSITION
  })
};

const setRobotFinalPosition = (s, { robotFinalPosition }) => {
  const [ _, ...robotsToExecute ] = s.robotsToExecute;

  return {
    ...s,
    robotsToExecute,
    executedRobots: [ ...s.executedRobots, robotFinalPosition ]
  }
};

const adjustRobotPosition = (s, { position }) => {
  const [ robotInExecution, ...robotsToExecute ] = s.robotsToExecute;

  return {
    ...s,
    robotsToExecute: [
      { 
        position,
        instructions: robotInExecution.instructions.slice(1)
      },
      ...robotsToExecute
    ]
  }
};

const reduceMap = {
  [ActionTypes.SET_ROBOT_FINAL_POSITION]: setRobotFinalPosition,
  [ActionTypes.ADJUST_ROBOT_POSITION]: adjustRobotPosition,
  [ActionTypes.SET_ROBOTS_TO_EXECUTE]: (s, { robotsToExecute }) => ({ ...s, robotsToExecute })
}

const getRobots = state => state.robots;
const getRobotsToExecute = state => getRobots(state).robotsToExecute;
const getRobotInExecution = state => getRobotsToExecute(state)[0];

const getNextMovement = state => {
  const { position, instructions } = getRobotInExecution(state);

  return {
    position,
    instruction: instructions[0]
  };
}

export const Selectors = {
  getRobots,
  getRobotsToExecute,
  getRobotInExecution,
  getNextMovement,
  getExecutedRobots: state => getRobots(state).executedRobots,
  isRobotLastInstruction: state => getRobotInExecution(state).instructions.length === 1,
  hasRobotsToExecute: state => getRobotsToExecute(state).length > 0,
};

export const Reducer = reducerFactory({ executedRobots: [] }, reduceMap);
