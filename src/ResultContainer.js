import React, { Component } from 'react';

import { Reducer as RobotsReducer, ActionCreators as RobotsActionCreators, Selectors as RobotSelectors } from './state/robots';
import { Reducer as GridReducer, ActionCreators as GridActionCreators } from './state/grid';
import { parseInput, stringifyOutput } from './parsers/textParser';
import InstructionExecutor from './controls/InstructionExecutor';

import './ResultContainer.css';

export default class ResultContainer extends Component {
  state = { simulationResult: null }
  
  getSimulationParams = props => ({
    robots: RobotsReducer(
      undefined,
      RobotsActionCreators.setRobotsToExecute(props.instructions.robots)
    ),
    grid: GridReducer(
      undefined,
      GridActionCreators.setDimensions(props.instructions.grid)
    )
  })

  simulate = props => {
    const instructionExecutor = new InstructionExecutor(
      this.getSimulationParams(props)
    );

    instructionExecutor.execute();

    this.setState(
      { simulationResult: RobotSelectors.getExecutedRobots(instructionExecutor.state) }
    );
  }

  componentDidMount() {
    this.simulate(this.props);
  }

  componentWillReceiveProps(props) {
    this.simulate(props);
  }

  render() {
    return (
      <div className="result-box">
        {
          this.state.simulationResult &&
            stringifyOutput(this.state.simulationResult).map(r => <div>{ r }</div>) 
        }
      </div>
    );
  }
}