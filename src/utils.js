export const chunk = (collection, size) => {
  let i = 0;
  let chuncked = [];

  while (i < collection.length) {
    chuncked = [
      ...chuncked,
      collection.slice(i, i + size)
    ]
    
    i += size;
  }

  return chuncked;
};

export const reducerFactory = (initialState, reduceMap) => (state = initialState, { type, ...payload }) => {
  return reduceMap[type] ? reduceMap[type](state, payload) : state
};
