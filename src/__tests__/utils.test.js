import { chunk } from '../utils';

describe('utils', () => {
  test('should create correct chuncks for perfect size match', () => {
    expect(
      chunk([1, 2, 3, 4, 5, 6, 7, 8], 2)
    ).toEqual([[1, 2], [3, 4], [5, 6], [7, 8]])
  });

  test('should create correct chuncks for unperfect size match', () => {
    expect(
      chunk([1, 2, 3, 4, 5, 6, 7, 8], 3)
    ).toEqual([[1, 2, 3], [4, 5, 6], [7, 8]])
  });
});
