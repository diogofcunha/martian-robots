import { chunk } from '../utils';

const parseGridInput = gridInput => {
  const [ numberOfColumns, numberOfRows ] = gridInput.split(' ').map(Number);

  return {
    numberOfColumns,
    numberOfRows
  }
};

const parseRobotInputs = robotInputs => chunk(robotInputs, 2).reduce((robots, [ pos, inst ]) => {
  const [ x, y, direction ] = pos.split(' ');

  return [
    ...robots,
    {
      position: { x: Number(x), y: Number(y), direction },
      instructions: [ ...inst]
    }
  ]
}, []);

export const parseInput = input => {
  const [ gridInput, ...robotInputs ] = input.split(/\r?\n/).filter(x => x);

  return {
    grid: parseGridInput(gridInput),
    robots: parseRobotInputs(robotInputs)
  }
};

export const stringifyOutput = (output) => output.map(pos => {
  const { x, y, direction, lost } = pos;

  return `${x} ${y} ${direction}${lost ? ' LOST' : ''}`;
});
