import { parseInput, stringifyOutput } from '../textParser';
import { parsedInput, textInput, textOutPut } from '../../testData/inputs';
import { DIRECTIONS } from '../../AppConstants';

describe('textParser', () => {
  test('should create a parsed shape', () => {
    expect(
      parseInput(textInput)
    ).toEqual(parsedInput)
  });

  test('should create a parsed shape', () => {
    expect(
      stringifyOutput([
        { x: 1, y: 1, direction: DIRECTIONS.EAST },
        { x: 3, y: 3, direction: DIRECTIONS.NORTH, lost: true },
        { x: 2, y: 3, direction: DIRECTIONS.SOUTH }
      ])
    ).toEqual(textOutPut);
  });
});
