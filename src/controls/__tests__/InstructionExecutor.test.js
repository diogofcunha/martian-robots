import InstructionExecutor from '../InstructionExecutor';
import { generatedState } from '../../testData/inputs';

describe('InstructionExecutor', () => {
  const getSampleState = 
  test('should execute as expected', () => {
    const executor = new InstructionExecutor(generatedState);

    executor.execute();

    expect(
      executor.state
    ).toMatchSnapshot()
  });
});
