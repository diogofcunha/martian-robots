import { getNextPosition } from '../steeringWheel';
import { DIRECTIONS, INSTRUCTION } from '../../AppConstants';

describe('steeringWheel', () => {
  const getPosition = override => ({ x: 10, y: 10, direction: DIRECTIONS.NORTH, ...override });

  describe('getNextPosition', () => {
    describe('when moving forward', () => {
      const moveForward = pos => getNextPosition(pos, INSTRUCTION.FORWARD)

      test('should move North', () => {
        expect(
          moveForward(getPosition())
        ).toMatchSnapshot();
      });

      test('should move South', () => {
        expect(
          moveForward(getPosition({ direction: DIRECTIONS.SOUTH }))
        ).toMatchSnapshot();
      });

      test('should move East', () => {
        expect(
          moveForward(getPosition({ direction: DIRECTIONS.EAST }))
        ).toMatchSnapshot();
      });

      test('should move WEST', () => {
        expect(
          moveForward(getPosition({ direction: DIRECTIONS.WEST }))
        ).toMatchSnapshot();
      });
    });

    describe('when changing direction', () => {
      describe('to the left', () => {
        const turnLeft = pos => getNextPosition(pos, INSTRUCTION.LEFT);

        test('from North, should go West', () => {
          expect(
            turnLeft(getPosition())
          ).toMatchSnapshot();
        });

        test('from South, should go East', () => {
          expect(
            turnLeft(getPosition({ direction: DIRECTIONS.SOUTH }))
          ).toMatchSnapshot();
        });

        test('from West, should go South', () => {
          expect(
            turnLeft(getPosition({ direction: DIRECTIONS.WEST }))
          ).toMatchSnapshot();
        });

        test('from East, should go North', () => {
          expect(
            turnLeft(getPosition({ direction: DIRECTIONS.EAST }))
          ).toMatchSnapshot();
        });
      });

      describe('to the right', () => {
        const turnRight = pos => getNextPosition(pos, INSTRUCTION.RIGHT);

        test('from North, should go East', () => {
          expect(
            turnRight(getPosition())
          ).toMatchSnapshot();
        });

        test('from South, should go West', () => {
          expect(
            turnRight(getPosition({ direction: DIRECTIONS.SOUTH }))
          ).toMatchSnapshot();
        });

        test('from West, should go North', () => {
          expect(
            turnRight(getPosition({ direction: DIRECTIONS.WEST }))
          ).toMatchSnapshot();
        });

        test('from East, should go South', () => {
          expect(
            turnRight(getPosition({ direction: DIRECTIONS.EAST }))
          ).toMatchSnapshot();
        });
      });
    });
  });
});
