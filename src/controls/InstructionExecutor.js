import {
  Reducer as RobotsReducer,
  Selectors as RobotSelectors,
  ActionCreators as RobotsActionCreators } from '../state/robots';

import {
  Reducer as GridReducer,
  Selectors as GridSelectors,
  ActionCreators as GridActionCreators } from '../state/grid';

import { getNextPosition, isForwardInstruction } from './steeringWheel';

export default class InstructionExecutor {
  constructor(state) {
    this.state = state;
  }

  _setPosition = position => {
    return RobotSelectors.isRobotLastInstruction(this.state) ?
      RobotsReducer(
        RobotSelectors.getRobots(this.state),
        RobotsActionCreators.setFinalPosition(position)
      ) :
      RobotsReducer(
        RobotSelectors.getRobots(this.state),
        RobotsActionCreators.ajustCurrentPosition(position)
      )
  }

  _setRobotLost = lastKnowPosition => {
    return {
      robots: RobotsReducer(
        RobotSelectors.getRobots(this.state),
        RobotsActionCreators.setFinalPosition({ ...lastKnowPosition, lost: true })
      ),
      grid: GridReducer(
        GridSelectors.getGrid(this.state),
        GridActionCreators.setBlackListedCoordinate(lastKnowPosition)
      )
    }
  }

  _calculateNextPosition = (instruction, currentPosition) => isForwardInstruction(instruction) &&
    GridSelectors.isMovementBlackListed(this.state, currentPosition, instruction) ?
      currentPosition :
      getNextPosition(currentPosition, instruction);

  
  execute = () => {  
    while (RobotSelectors.hasRobotsToExecute(this.state)) {
      const { position, instruction } = RobotSelectors.getNextMovement(this.state);

      const nextPosition = this._calculateNextPosition(instruction, position);

      const wasRobotLost = !GridSelectors.isInsideGridBounderies(this.state, nextPosition);
  
      this.state = {
        ...this.state,
        ...wasRobotLost ?
          this._setRobotLost(position) :
          { robots: this._setPosition(nextPosition) }
      };
    }
  }
}
