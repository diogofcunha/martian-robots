import { DIRECTIONS, INSTRUCTION, DIRECTION_CLOCK } from '../AppConstants';

const POSITION_REDUCERS_BY_DIRECTION = {
  [DIRECTIONS.NORTH]: ({ y }) => ({ y: y + 1 }),
  [DIRECTIONS.SOUTH]: ({ y }) => ({ y: y - 1 }),
  [DIRECTIONS.WEST]: ({ x }) => ({ x: x - 1 }),
  [DIRECTIONS.EAST]: ({ x }) => ({ x: x + 1 })
}

const move = currentPosition => ({
  ...currentPosition,
  ...POSITION_REDUCERS_BY_DIRECTION[currentPosition.direction](currentPosition)
});

const getCurrentClockIndex = direction => DIRECTION_CLOCK.indexOf(direction);
const getClockIndexAccumulator = (i, instruction) => instruction === INSTRUCTION.LEFT ? i - 1 : i + 1;  

const getNextDirection = (currentDirection, instruction) => {
  const candidateDirectionIndex = getClockIndexAccumulator(
    getCurrentClockIndex(currentDirection),
    instruction
  );

  const directionIndex = candidateDirectionIndex < 0 ? DIRECTION_CLOCK.length - 1 : candidateDirectionIndex % DIRECTION_CLOCK.length;

  return DIRECTION_CLOCK[directionIndex];
}

const turn = (currentPosition, instruction) => ({
  ...currentPosition,
  direction: getNextDirection(currentPosition.direction, instruction)
});

export const isForwardInstruction = instruction => instruction === INSTRUCTION.FORWARD;

export const getNextPosition = (currentPosition, instruction) => isForwardInstruction(instruction) ?
  move(currentPosition) :
  turn(currentPosition, instruction);
