import { DIRECTIONS, INSTRUCTION } from '../AppConstants';

export const textInput = "5 3\n1 1 E\nRFRFRFRF\n3 2 N\nFRRFLLFFRRFLL\n0 3 W\nLLFFFLFLFL";

export const parsedInput = {
  grid: {
    numberOfColumns: 5,
    numberOfRows: 3
  },
  robots: [
    {
      position: { x: 1, y: 1, direction: DIRECTIONS.EAST },
      instructions: [
        INSTRUCTION.RIGHT,
        INSTRUCTION.FORWARD,
        INSTRUCTION.RIGHT,
        INSTRUCTION.FORWARD,
        INSTRUCTION.RIGHT,
        INSTRUCTION.FORWARD,
        INSTRUCTION.RIGHT,
        INSTRUCTION.FORWARD
      ]
    },
    {
      position: { x: 3, y: 2, direction: DIRECTIONS.NORTH },
      instructions: [
        INSTRUCTION.FORWARD,
        INSTRUCTION.RIGHT,
        INSTRUCTION.RIGHT,
        INSTRUCTION.FORWARD,
        INSTRUCTION.LEFT,
        INSTRUCTION.LEFT,
        INSTRUCTION.FORWARD,
        INSTRUCTION.FORWARD,
        INSTRUCTION.RIGHT,
        INSTRUCTION.RIGHT,
        INSTRUCTION.FORWARD,
        INSTRUCTION.LEFT,
        INSTRUCTION.LEFT
      ]
    },
    {
      position: { x: 0, y: 3, direction: DIRECTIONS.WEST },
      instructions: [
        INSTRUCTION.LEFT,
        INSTRUCTION.LEFT,
        INSTRUCTION.FORWARD,
        INSTRUCTION.FORWARD,
        INSTRUCTION.FORWARD,
        INSTRUCTION.LEFT,
        INSTRUCTION.FORWARD,
        INSTRUCTION.LEFT,
        INSTRUCTION.FORWARD,
        INSTRUCTION.LEFT
      ]
    }
  ]
}

export const textOutPut = ['1 1 E', '3 3 N LOST', '2 3 S'];

export const generatedState = {
  "robots": {
    "executedRobots": [],
    "robotsToExecute": [
      {
        "position": {
          "x": 1,
          "y": 1,
          "direction": "E"
        },
        "instructions": [
          "R",
          "F",
          "R",
          "F",
          "R",
          "F",
          "R",
          "F"
        ]
      },
      {
        "position": {
          "x": 3,
          "y": 2,
          "direction": "N"
        },
        "instructions": [
          "F",
          "R",
          "R",
          "F",
          "L",
          "L",
          "F",
          "F",
          "R",
          "R",
          "F",
          "L",
          "L"
        ]
      },
      {
        "position": {
          "x": 0,
          "y": 3,
          "direction": "W"
        },
        "instructions": [
          "L",
          "L",
          "F",
          "F",
          "F",
          "L",
          "F",
          "L",
          "F",
          "L"
        ]
      }
    ]
  },
  "grid": {
    "blackListedCoordinates": [],
    "minY": 0,
    "minX": 0,
    "maxY": 3,
    "maxX": 5
  }
};
