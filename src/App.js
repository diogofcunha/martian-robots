import React, { Component } from 'react';

import { parseInput } from './parsers/textParser';
import ResultContainer from './ResultContainer';

import logo from './planet.svg';
import './App.css';

class App extends Component {
  state = { instructionsText: '', instructions: null }

  render() {
    return (
      <div>
        <div className="banner"></div>
        <h2 className="app-title">MARTIAN ROBOT CONTROLLER</h2>
        <div className="app-container">
          <div className="app-execution-container">
            <textarea
              className="app-input-box"
              value={this.state.instructionsText}
              onChange={e => { this.setState({ instructionsText: e.target.value }) }}
            />
            <div className="app-simulate">
              <button
                onClick={() => {
                  this.setState({ instructions: parseInput(this.state.instructionsText)})
                }}
              >
                Simulate >
              </button>
            </div>
          </div>
          <div className="app-execution-result">
            {
              this.state.instructions &&
                <ResultContainer
                  instructions={this.state.instructions}
                  delay={0}
                />
            }
          </div>
        </div>
      </div>
    );
  }
}

export default App;
